import 'package:flutter/material.dart';

class LikeButtonList extends StatefulWidget {
  final int itemCount;
  final ValueChanged<int> onLiked;

  LikeButtonList({
    Key? key,
    required this.itemCount,
    required this.onLiked,
  }) : super(key: key);

  @override
  _LikeButtonListState createState() => _LikeButtonListState();
}

class _LikeButtonListState extends State<LikeButtonList> {
   late List<bool> _isLikedList;

  @override
  void initState() {
    super.initState();
    _isLikedList = List.generate(widget.itemCount, (_) => false);
  }

  void _toggleLike(int index) {
    setState(() {
      _isLikedList[index] = !_isLikedList[index];
    });
    widget.onLiked(index);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (int i = 0; i < widget.itemCount; i++)
          IconButton(
            icon: Icon(
              _isLikedList[i] ? Icons.favorite : Icons.favorite_border,
              color: _isLikedList[i] ? Colors.red : Colors.grey,
            ),
            onPressed: () => _toggleLike(i),
          ),
      ],
    );
  }
}
