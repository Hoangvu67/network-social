import 'dart:convert';
import 'package:demo_social_network/base_bloc/base_state.dart';
import 'package:demo_social_network/model/post_model.dart';
import 'package:demo_social_network/model/response_model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';


class RestAPI extends Cubit<BaseState>{
  RestAPI() : super(InitState());

  final String accessToken = '118a023dd394a879c75850b878af56a0c48c6e64a5b383095d3b8e5dd1f9a65e';
  Future<void> getAPI() async {
    emit(LoadingState());
    try{
      var url = Uri.parse(
          'https://gorest.co.in/public/v2/posts');
      var headers = {'Authorization': 'Bearer $accessToken'};
      // Await the http get response, then decode the json-formatted response.
      final response = await http.get(url, headers: headers);
      // final Map<String, dynamic> parsed = json.decode(response.body);
      List<PostModel> list = (json.decode(response.body) as List).map((data) => PostModel.fromJson(data)).toList();
      emit(LoadedState(list));
    }
    catch(e){
      emit(ErrorState(e.toString()));
    }

  }

  Future<ResponseModel> postAPI(PostModel postModel) async {
    var url = Uri.parse('https://gorest.co.in/public/v2/users/723302/posts');
    var headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json'
    };
    var bodyData = postModel.toMap();
    var bodyJson = json.encode(bodyData);
    final response = await http.post(url, headers: headers, body: bodyJson);
    if (response.statusCode == 201) {
      return ResponseModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to post data');
    }
  }

  Future<void> updatePost(PostModel postModel) async {
    final url = Uri.parse('https://gorest.co.in/public/v2/posts/${postModel.id}');
    final headers = {
      'Authorization': 'Bearer $accessToken',
      'Content-Type': 'application/json'
    };
    final bodyData = postModel.toMap();
    final bodyJson = json.encode(bodyData);
    final response = await http.put(
      url,
      headers: headers,
      body: bodyJson,
    );
    if (response.statusCode == 200) {
      print('Post updated successfully!');
    } else {
      throw Exception('Failed to update post.');
    }
  }
  Future<void> deletePost(PostModel postModel) async {
    final url =
    Uri.parse('https://gorest.co.in/public/v2/posts/${postModel.id}');
    final headers = {
      'Authorization': 'Bearer $accessToken',
    };
    final response = await http.delete(url, headers: headers);
    if (response.statusCode == 204) {
      print('Post deleted successfully!');
    } else {
      throw Exception('Failed to delete post.');
    }
  }
}


