import 'package:flutter/material.dart';

class AppColors {
  // define all your color
  static const Color white = Color(0xffffffff);
  static const Color base_color = Color(0xFF0085FF);
  static const Color base_color_border_textfield = Color(0xFFD6DCE2);
  static const Color gray = Color(0xFF75818F);
  static const Color border = Color(0xFFD4CFCF);
  static const Color gray4 = Color(0xFFF3F1F1);
  static const Color gray1 = Color(0xFFBDBDBD);
  static const Color gray2 = Color(0xFF828282);
  static const Color gray3 = Color(0xFF545252);
  static const Color disable = Color(0xffF0F4F9);
  static const Color green1 = Color(0xffd0efd3);
  static const Color green2 = Color.fromRGBO(5, 140, 14, 1.0);
  static const Color green3 = Color.fromRGBO(15, 162, 25, 1.0);
  static const Color black = Color(0xff262626);
  static const Color blue1 = Color(0xFF81B8E3);
  static const Color blue4 = Color(0xFF3899E5);
  static const Color blue5 = Color(0xFF0D49FF);
  static const Color orange = Color(0xFFFFA007);
  static const Color pink = Colors.pinkAccent;

  static const LinearGradient base_color_gradient = LinearGradient(colors: [
    Color(0xFF258EFF),
    AppColors.blue5,
  ], begin: FractionalOffset.topCenter, end: FractionalOffset.bottomCenter, tileMode: TileMode.mirror);
}
