import 'package:demo_social_network/Screen_model/Post_Screen.dart';
import 'package:demo_social_network/Screen_model/upload_screen.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: PostScreen()
    );
  }
}

