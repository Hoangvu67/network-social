import 'package:demo_social_network/res/dimens.dart';
import 'package:flutter/material.dart';


class BaseScreen extends StatelessWidget {
  static double toolbarHeight = 50;

  // body của màn hình
  final Widget? body;

  // title của appbar có 2 kiểu String và Widget
  // title là kiểu Widget thì sẽ render widget
  // title là String
  final dynamic title;

  // trường hợp có AppBar đặc biệt thì dùng customAppBar
  final Widget? customAppBar;

  // callBack của onBackPress với trường hợp  hiddenIconBack = false
  final Function? onBackPress;

  // custom widget bên phải của appBar
  final List<Widget>? rightWidgets;

  // loadingWidget để show loading toàn màn hình
  final Widget? loadingWidget;

  // show thông báo
  final Widget? messageNotify;
  final Widget? floatingButton;
  final Widget? customBottomNavigationBar;
  // nếu true => sẽ ẩn backIcon , mặc định là true
  final bool hiddenIconBack;
  final bool hideNavigationBar;

  final Color colorTitle;
  final Color? iconBackColor;
  final bool hideAppBar;
  final Color? backgroundColor;
  final bool extendbody;
  final bool? resizeToAvoidBottomInset;

  const BaseScreen(
      {Key? key,
        this.body,
        this.title = "",
        this.customAppBar,
        this.extendbody = false,
        this.resizeToAvoidBottomInset,
        this.backgroundColor,
        this.onBackPress,
        this.rightWidgets,
        this.hiddenIconBack = false,
        this.colorTitle = Colors.white,
        this.loadingWidget,
        this.iconBackColor,
        this.hideAppBar = false,
        this.messageNotify,
        this.customBottomNavigationBar,
        this.hideNavigationBar = true,
        this.floatingButton})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final scaffold = Scaffold(
      appBar: hideAppBar ? null : (customAppBar == null ? baseAppBar(context) : customAppBar),
      backgroundColor: backgroundColor ?? Colors.transparent,
      extendBody: extendbody,
      resizeToAvoidBottomInset: resizeToAvoidBottomInset,
      body: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Stack(
          children: [
            body ?? Container(),
            Positioned(
              top: AppDimens.SIZE_0,
              right: AppDimens.SIZE_0,
              left: AppDimens.SIZE_0,
              bottom: AppDimens.SIZE_0,
              child: loadingWidget ?? Container(),
            ),
            messageNotify ?? Container()
          ],
        ),
      ),
      floatingActionButton: floatingButton ?? null,
      bottomNavigationBar: customBottomNavigationBar,
    );

    return Stack(
      children: [
        // Positioned.fill(child: Container(color: backgroundColor,)),
        // Container(
        //   child: Image.asset(
        //     AppImages.logo_splash,
        //     width: MediaQuery.of(context).size.width,
        //     height: MediaQuery.of(context).size.height,
        //     fit: BoxFit.fill,
        //   ),
        // ),
        scaffold
      ],
    );
  }

  baseAppBar(BuildContext context) {
    var widgetTitle;
    if (title is Widget) {
      widgetTitle = title;
    } else {
      widgetTitle =
      //     CustomTextLabel(
      //   this.title?.toString(),
      //   maxLines: 2,
      //   fontWeight: FontWeight.w700,
      //   fontSize: 20.sw,
      //   textAlign: TextAlign.center,
      //   color: colorTitle,
      // );
      Text(this.title.toString());
    }
    return AppBar(
      elevation: 0,
      toolbarHeight: toolbarHeight,
      backgroundColor: Colors.transparent,
      title: widgetTitle,
      leading: hiddenIconBack
          ? Container()
          : InkWell(
        onTap: () {
          Navigator.pop(context);
          onBackPress?.call();
        },
        child: Container(
          width: 50,
          alignment: Alignment.center,
          child: Icon(Icons.arrow_back_ios)
        ),
      ),
      centerTitle: true,
      actions: rightWidgets ?? [],
    );
  }
  baseBottomNavigationBar(BuildContext context) {

  }
}
