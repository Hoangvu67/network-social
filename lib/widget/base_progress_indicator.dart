import 'package:flutter/material.dart';


class BaseProgressIndicator extends StatelessWidget {
  final double? size;

  const BaseProgressIndicator({Key? key, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loading = CircularProgressIndicator(
      strokeWidth: 3,
      backgroundColor: Colors.grey.withOpacity(0.3),
      valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),
    );
    return size == null
        ? loading
        : SizedBox(
            width: size,
            height: size,
            child: loading,
          );
  }
}
