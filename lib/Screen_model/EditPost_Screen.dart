import 'package:demo_social_network/Screen_model/Post_Screen.dart';
import 'package:demo_social_network/controller/rest_api.dart';
import 'package:demo_social_network/model/post_model.dart';
import 'package:flutter/material.dart';

class EditPostScreen extends StatefulWidget {
  final PostModel post;

  const EditPostScreen({Key? key, required this.post}) : super(key: key);

  @override
  _EditPostScreenState createState() => _EditPostScreenState();
}

class _EditPostScreenState extends State<EditPostScreen> {
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _bodyController = TextEditingController();
  final restAPI = RestAPI();

  @override
  void initState() {
    super.initState();
    _titleController.text = widget.post.title ?? '';
    _bodyController.text = widget.post.body ?? '';
  }

  @override
  void dispose() {
    _titleController.dispose();
    _bodyController.dispose();
    super.dispose();
  }

  getData() async {
    // listData = await RestAPI().getAPI();
    setState(() {});
  }

  List<PostModel> listData = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Post'),
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextFormField(
              controller: _titleController,
              decoration: InputDecoration(
                labelText: 'Title',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 10),
            TextFormField(
              controller: _bodyController,
              maxLines: null,
              keyboardType: TextInputType.multiline,
              decoration: InputDecoration(
                labelText: 'Body',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: () async {
                try {
                  final UpdatePost = PostModel(
                      id: widget.post.id,
                      title: _titleController.text,
                      body: _bodyController.text,
                      user_id: widget.post.user_id);
                  final responseModel = await restAPI.updatePost(UpdatePost);
                  Navigator.of(context).pop("success");
                } catch (e) {
                  print(e);
                  // Show error message
                }
              },
              child: Text('Save'),
            ),
          ],
        ),
      ),
    );
  }
}
