import 'package:demo_social_network/Screen_model/home_page.dart';
import 'package:demo_social_network/Screen_model/upload_screen.dart';
import 'package:demo_social_network/base_bloc/base.dart';
import 'package:demo_social_network/controller/Like_Button.dart';
import 'package:demo_social_network/model/post_model.dart';
import 'package:demo_social_network/widget/base_loading.dart';
import 'package:demo_social_network/widget/base_screen.dart';
import 'package:flutter/material.dart';
import 'package:demo_social_network/controller/rest_api.dart';
import 'package:demo_social_network/model/response_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'dart:convert' as convert;
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';
import 'package:demo_social_network/Screen_model/Post_Screen.dart';
import 'EditPost_Screen.dart';

class PostScreen extends StatelessWidget {
  const PostScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => RestAPI(),
      child: PostScreenBody(),
    );
  }
}

class PostScreenBody extends StatefulWidget {
  const PostScreenBody({Key? key}) : super(key: key);

  @override
  State<PostScreenBody> createState() => _PostScreenState();
}

class _PostScreenState extends State<PostScreenBody> {
  ResponseModel responseModel = new ResponseModel();
  List<bool> _isLikedList = [false, false, false];
  int _commentCount = 0;
  List<PostModel> listData = [];

  void _handleLiked(int index) {
    setState(() {
      _isLikedList[index] = !_isLikedList[index];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    context.read<RestAPI>().getAPI();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return BaseScreen(
        hideAppBar: true,
        backgroundColor: Colors.white,
        loadingWidget: CustomLoading<RestAPI>(),
        body: SingleChildScrollView(
          child: Column(
              children: [
            BlocBuilder<RestAPI,BaseState>(builder: (context,state){
              if(state is LoadedState){
                  listData = state.data;
                  return ListView.builder(
                    shrinkWrap: true,
                    itemCount: listData.length,
                    itemBuilder: (context, index) {
                      return Card(
                        margin: EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ListTile(
                              leading: CircleAvatar(
                                backgroundImage:
                                NetworkImage('https://via.placeholder.com/150'),
                              ),
                              title: Text(listData[index].id.toString() ?? ""),
                              subtitle: Text("2 Hour ago"),
                            ),
                            Padding(
                              padding: EdgeInsets.all(10),
                              child: Text(listData[index].body ?? ""),
                            ),
                            Row(
                              children: [
                                LikeButtonList(
                                  itemCount: 1,
                                  onLiked: _handleLiked,
                                ),
                                IconButton(
                                    icon: Icon(Icons.comment),
                                    onPressed: () {
                                      Navigator.of(context).push(MaterialPageRoute(
                                          builder: (context) => Homepage(
                                            id: listData[index].id,
                                          )));
                                    }),
                                Text('$_commentCount'),
                                IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () async {
                                      var result = await Navigator.of(context).push(
                                          MaterialPageRoute(
                                              builder: (context) => EditPostScreen(
                                                  post: listData[index])));
                                      if (result == "success") {
                                        getData();
                                      }
                                    }),
                                IconButton(
                                  icon: Icon(Icons.delete),
                                  onPressed: () {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Text('Delete post'),
                                          content: Text(
                                              'Are you sure you want to delete this post?'),
                                          actions: [
                                            TextButton(
                                              onPressed: () =>
                                                  Navigator.of(context).pop(),
                                              child: Text('Cancel'),
                                            ),
                                            TextButton(
                                              onPressed: () async {
                                                final responseModel = await RestAPI()
                                                    .deletePost(listData[index]);
                                                Navigator.of(context).pop();
                                                getData();
                                              },
                                              child: Text('Delete'),
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  );
              }
              else if (state is LoadingState){
                Container(
                  height: 400,
                  width: 300,
                  child: CircularProgressIndicator(),
                );
              }
              return Container();
            }),
          ]),
        ),
        floatingButton: FloatingActionButton(
          onPressed: () async {
            var result = await Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Upload()),
            );
            if (result == "Success") {
              getData();
            }
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ));
  }
}
